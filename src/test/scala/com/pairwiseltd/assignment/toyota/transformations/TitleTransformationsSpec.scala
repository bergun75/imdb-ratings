package com.pairwiseltd.assignment.toyota.transformations

import com.holdenkarau.spark.testing.DatasetSuiteBase
import com.pairwiseltd.assignment.toyota.generators.{EnrichedRawTitleWithRankingGen, EnrichedTitleGen, RatingGen}
import org.apache.spark.SparkConf
import org.scalacheck.Gen
import org.scalacheck.Prop.{forAll, _}
import org.scalatest.wordspec.AnyWordSpec
import org.scalatestplus.scalacheck.Checkers

class TitleTransformationsSpec extends AnyWordSpec with DatasetSuiteBase with Checkers
  with EnrichedTitleGen with RatingGen with EnrichedRawTitleWithRankingGen {
  override def conf: SparkConf = super.conf.set("spark.sql.shuffle.partitions", "1")

  val limit: Int = Gen.chooseNum(5, 10).sample.getOrElse(8)
  val evenLimit: Int = Gen.chooseNum(5, 10).suchThat(_ % 2 == 0).sample.getOrElse(8)
  val minVotesToConsider = 500

  "TitlesTransformations" when {
    "aggregate called with all top x(=topNumber) ratingsFromStream rankings smaller than all top x(=topNumber) " +
      "staticTitles rankings" should {
      "always return the input same as topStaticTitles" in {
        spark.sparkContext.setLogLevel("WARN")
        import spark.implicits._

        val transformations = new TitleTransformations(spark, limit, minVotesToConsider)

        val combinedGenerators =
          Gen.zip(listOfNRatingGen(limit),
            listOfNEnrichedRawTitleWithRankingGen(limit),
            listOfNRatingGen(limit * 2))
        val property = forAll(combinedGenerators) {
          case (newRatings, existingTopTitles, allRatingsForAverageVoteStatistics) =>
            val imposedNewRatings = newRatings.map(x => x.copy(averageRating = 8.3, numVotes = 1001))
            val imposedExistingTopTitles =
              existingTopTitles.map(x => x.copy(ranking = 8.4, averageRating = 8.4, numVotes = 1000)) ++
                imposedNewRatings.map(x => getEnrichedRawTitleWithRankingGen(x))
            val imposedAllRatings = (imposedNewRatings.map(_.tconst) ++ existingTopTitles.map(_.tconst))
              .zip(allRatingsForAverageVoteStatistics)
              .map { case (tconst, rating) => rating.copy(tconst = tconst, averageRating = 8.4, numVotes = 1000) }
            // imposedAllRatings mathematically this may not be correct on having such an aggregation
            // but I am assuming it was like this.
            val imposedNewRatingsDs = spark.createDataset(imposedNewRatings)
            val imposedExistingTopTitlesDs = spark.createDataset(imposedExistingTopTitles)
            val imposedAverageVotesStatistics = transformations.sumOfVotes(spark.createDataset(imposedAllRatings))
            val actual =
              transformations.aggregateStreamBatch(imposedNewRatingsDs,
                imposedExistingTopTitlesDs,
                imposedAverageVotesStatistics)
            actual.count == limit &&
              existingTopTitles.map(_.tconst).toSet
                .intersect(actual.collect.map(_.tconst).toSet).size == limit &&
              imposedNewRatingsDs.collect.map(_.tconst).toSet
                .intersect(actual.collect.map(_.tconst).toSet).isEmpty
        }
        check(property)
      }
    }
    "aggregate called with all top x(=topNumber) ratingsFromStream rankings bigger than all x(=topNumber) " +
      "topStaticTitles rankings" should {
      "always return the title ids same as topRatingsFromStream" in {
        spark.sparkContext.setLogLevel("WARN")
        import spark.implicits._
        val transformations = new TitleTransformations(spark, limit, minVotesToConsider)
        val combinedGenerators =
          Gen.zip(listOfNRatingGen(limit),
            listOfNEnrichedRawTitleWithRankingGen(limit),
            listOfNRatingGen(limit * 2))
        val property = forAll(combinedGenerators) {
          case (newRatings, existingTopTitles, allRatingsForAverageVoteStatistics) =>
            val imposedNewRatings = newRatings.map(x => x.copy(averageRating = 8.5, numVotes = 1002))
            val imposedExistingTopTitles =
              existingTopTitles.map(x => x.copy(ranking = 8.4, averageRating = 8.4, numVotes = 1000)) ++
                imposedNewRatings.map(x => getEnrichedRawTitleWithRankingGen(x))
            val imposedAllRatings = (imposedNewRatings.map(_.tconst) ++ existingTopTitles.map(_.tconst))
              .zip(allRatingsForAverageVoteStatistics)
              .map { case (tconst, rating) => rating.copy(tconst = tconst, averageRating = 8.4, numVotes = 1000) }
            // imposedAllRatings mathematically this may not be correct on having such an aggregation
            // but I am assuming it was like this.
            val imposedNewRatingsDs = spark.createDataset(imposedNewRatings)
            val imposedExistingTopTitlesDs = spark.createDataset(imposedExistingTopTitles)
            val imposedAverageVotesStatistics = transformations.sumOfVotes(spark.createDataset(imposedAllRatings))
            val actual =
              transformations.aggregateStreamBatch(imposedNewRatingsDs,
                imposedExistingTopTitlesDs,
                imposedAverageVotesStatistics)
            actual.count == limit &&
              existingTopTitles.map(_.tconst).toSet
                .intersect(actual.collect.map(_.tconst).toSet).isEmpty &&
              imposedNewRatingsDs.collect.map(_.tconst).toSet
                .intersect(actual.collect.map(_.tconst).toSet).size == limit
        }
        check(property)
      }
    }
    "aggregate called with all top x(=topNumber) ratingsFromStream numVotes smaller than minVotesToConsider" should {
      "always return the input same as topStaticTitles" in {
        spark.sparkContext.setLogLevel("WARN")
        import spark.implicits._
        val transformations = new TitleTransformations(spark, limit, minVotesToConsider)
        val combinedGenerators =
          Gen.zip(listOfNRatingGen(limit),
            listOfNEnrichedRawTitleWithRankingGen(limit),
            listOfNRatingGen(limit * 2))
        val property = forAll(combinedGenerators) {
          case (newRatings, existingTopTitles, allRatingsForAverageVoteStatistics) =>
            val imposedNewRatings = newRatings.map(x => x.copy(averageRating = 8.5, numVotes = 459)) ++
              newRatings.map(x => x.copy(averageRating = 8.3, numVotes = 458)) // more than 1 rating case for 1 title
            val imposedExistingTopTitles =
              existingTopTitles.map(x => x.copy(ranking = 8.4, averageRating = 8.4, numVotes = 1000))
            val imposedAllRatings = (imposedNewRatings.map(x => (x.tconst, x.numVotes)) ++
              existingTopTitles.map(x => (x.tconst, x.numVotes))).zip(allRatingsForAverageVoteStatistics)
              .map { case ((tconst, votes), rating) =>
                rating.copy(tconst = tconst, averageRating = 8.4, numVotes = if (votes == 1000) votes else votes)
              }
            // imposedAllRatings mathematically this may not be correct on having such an aggregation
            // but I am assuming it was like this.
            val imposedNewRatingsDs = spark.createDataset(imposedNewRatings)
            val imposedExistingTopTitlesDs = spark.createDataset(imposedExistingTopTitles)
            val imposedAverageVotesStatistics = transformations.sumOfVotes(spark.createDataset(imposedAllRatings))
            val actual =
              transformations.aggregateStreamBatch(imposedNewRatingsDs,
                imposedExistingTopTitlesDs,
                imposedAverageVotesStatistics)
            actual.count == limit &&
              existingTopTitles.map(_.tconst).toSet
                .intersect(actual.collect.map(_.tconst).toSet).size == limit &&
              imposedNewRatingsDs.collect.map(_.tconst).toSet
                .intersect(actual.collect.map(_.tconst).toSet).isEmpty
        }
        check(property)
      }
    }
    "aggregate called with all top x(=evenTopNumber) ratingsFromStream rankings bigger than " +
      "some topStaticTitles rankings" should {
      "always return x(=evenTopNumber)/2 title ids from each datasets" in {
        spark.sparkContext.setLogLevel("WARN")
        import spark.implicits._
        val transformations = new TitleTransformations(spark, evenLimit, minVotesToConsider)
        val combinedGenerators =
          Gen.zip(listOfNRatingGen(evenLimit),
            listOfNEnrichedRawTitleWithRankingGen(evenLimit),
            listOfNRatingGen(evenLimit * 2))
        val property = forAll(combinedGenerators) {
          case (newRatings, existingTopTitles, allRatingsForAverageVoteStatistics) =>
          val imposedNewRatings = newRatings.zipWithIndex.map { case (x, idx) =>
            if (idx % 2 == 0) x.copy(averageRating = 8.5, numVotes = 1001)
            else x.copy(averageRating = 8.3, numVotes = 1001)
          }
          val imposedExistingTopTitles =
            existingTopTitles.map(x => x.copy(ranking = 8.4, averageRating = 8.4, numVotes = 1000)) ++
              imposedNewRatings.map(x => getEnrichedRawTitleWithRankingGen(x))
          val imposedAllRatings = (imposedNewRatings.map(_.tconst) ++ existingTopTitles.map(_.tconst)).zip(allRatingsForAverageVoteStatistics)
            .map { case (tconst, rating) => rating.copy(tconst = tconst, averageRating = 8.4, numVotes = 1000) }
          // imposedAllRatings mathematically this may not be correct on having such an aggregation
          // but I am assuming it was like this.
          val imposedNewRatingsDs = spark.createDataset(imposedNewRatings)
          val imposedExistingTopTitlesDs = spark.createDataset(imposedExistingTopTitles)
          val imposedAverageVotesStatistics = transformations.sumOfVotes(spark.createDataset(imposedAllRatings))
          val actual =
            transformations.aggregateStreamBatch(imposedNewRatingsDs,
              imposedExistingTopTitlesDs,
              imposedAverageVotesStatistics)
          actual.count == evenLimit &&
            existingTopTitles.map(_.tconst).toSet
              .intersect(actual.collect.map(_.tconst).toSet).size == evenLimit / 2 &&
            imposedNewRatingsDs.collect.map(_.tconst).toSet
              .intersect(actual.collect.map(_.tconst).toSet).size == evenLimit / 2
        }
        check(property)
      }
    }
  }
}
