package com.pairwiseltd.assignment.toyota.generators

import com.pairwiseltd.assignment.toyota.model.Rating
import org.scalacheck.Gen

trait RatingGen {
  def listOfNRatingGen(size: Int): Gen[List[Rating]] = Gen.listOfN(size, createRatingGen).suchThat(_.size == size)

  def createRatingGen: Gen[Rating] =
    for {
      tconst <- Gen.alphaStr.suchThat(!_.isEmpty)
      averageRating <- Gen.choose(1000, 2000).flatMap(x => Gen.choose(3, 5).map(y => x / y))
      numVotes <- Gen.choose(400, 100000)
    } yield Rating(
      tconst = tconst,
      averageRating = BigDecimal(averageRating),
      numVotes = numVotes
    )
}
