package com.pairwiseltd.assignment.toyota.generators

import com.pairwiseltd.assignment.toyota.model.EnrichedTitle
import org.scalacheck.Gen

trait EnrichedTitleGen {
  def listOfNEnrichedTitleGen(size: Int) = Gen.listOfN(size, createEnrichedTitleGen).suchThat(_.size == size)

  def createEnrichedTitleGen: Gen[EnrichedTitle] =
    for {
      tconst <- Gen.numStr.suchThat(!_.isEmpty)
      ranking <- Gen.chooseNum(5, 100).map(_.toDouble)
      primaryTitle <- Gen.alphaNumStr.suchThat(!_.isEmpty)
      originalTitle <- Gen.alphaNumStr.suchThat(!_.isEmpty)
      mostCreditedName <- Gen.alphaNumStr.suchThat(!_.isEmpty)
    } yield EnrichedTitle(
      tconst = tconst,
      ranking = BigDecimal(ranking),
      primaryTitle = primaryTitle,
      originalTitle = originalTitle,
      mostCreditedName = mostCreditedName
    )
}
