package com.pairwiseltd.assignment.toyota.generators

import com.pairwiseltd.assignment.toyota.model.{EnrichedRawTitle, EnrichedRawTitleWithRanking, Rating}
import org.scalacheck.Gen

trait EnrichedRawTitleWithRankingGen {
  def listOfNEnrichedRawTitleWithRankingGen(size: Int): Gen[List[EnrichedRawTitleWithRanking]] =
    Gen.listOfN(size, createEnrichedRawTitleWithRankingGen).suchThat(_.size == size)

  def createEnrichedRawTitleWithRankingGen: Gen[EnrichedRawTitleWithRanking] =
    for {
      tconst <- Gen.numStr.suchThat(!_.isEmpty)
      averageRating <- Gen.chooseNum(5, 100).map(_.toDouble)
      numVotes <- Gen.choose(400, 100000)
      ranking <- Gen.chooseNum(1000, 2000).map(_.toDouble)
      primaryTitle <- Gen.alphaNumStr.suchThat(!_.isEmpty)
      originalTitle <- Gen.alphaNumStr.suchThat(!_.isEmpty)
      mostCreditedName <- Gen.alphaNumStr.suchThat(!_.isEmpty)
    } yield EnrichedRawTitleWithRanking(
      tconst = tconst,
      averageRating = BigDecimal(averageRating),
      ranking = BigDecimal(ranking),
      numVotes = numVotes,
      primaryTitle = primaryTitle,
      originalTitle = originalTitle,
      mostCreditedName = mostCreditedName
    )

  def getEnrichedRawTitleWithRankingGen(rating: Rating): EnrichedRawTitleWithRanking =
    EnrichedRawTitleWithRanking(
      tconst = rating.tconst,
      averageRating = rating.averageRating,
      ranking = rating.averageRating,
      numVotes = rating.numVotes,
      primaryTitle = Gen.alphaNumStr.suchThat(!_.isEmpty).sample.getOrElse("asdasdas"),
      originalTitle = Gen.alphaNumStr.suchThat(!_.isEmpty).sample.getOrElse("asdasdas"),
      mostCreditedName = Gen.alphaNumStr.suchThat(!_.isEmpty).sample.getOrElse("asdasdas")
    )
}
