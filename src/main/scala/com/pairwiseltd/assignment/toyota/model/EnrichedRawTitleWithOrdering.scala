package com.pairwiseltd.assignment.toyota.model

case class EnrichedRawTitleWithOrdering(tconst: String, averageRating: BigDecimal, numVotes: Int, primaryTitle: String,
                                        originalTitle: String, ordering: Option[Int], primaryName: String)
