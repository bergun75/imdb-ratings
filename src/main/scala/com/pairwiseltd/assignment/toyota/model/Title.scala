package com.pairwiseltd.assignment.toyota.model

case class Title(tconst: String, titleType: String, primaryTitle: String, originalTitle: String)
