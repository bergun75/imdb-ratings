package com.pairwiseltd.assignment.toyota.model

case class EnrichedRawTitleWithRanking(tconst: String, averageRating: BigDecimal, numVotes: Int, ranking: BigDecimal,
                                       primaryTitle: String, originalTitle: String, mostCreditedName: String)

object EnrichedRawTitleWithRanking{
  def apply(title: EnrichedRawTitle, averageVote: BigDecimal): EnrichedRawTitleWithRanking =
    new EnrichedRawTitleWithRanking(
      tconst = title.tconst,
      averageRating = title.averageRating,
      numVotes = title.numVotes,
      ranking = title.numVotes * title.averageRating / averageVote,
      primaryTitle= title.primaryTitle,
      originalTitle = title.originalTitle,
      mostCreditedName = title.mostCreditedName)
}