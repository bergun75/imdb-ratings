package com.pairwiseltd.assignment.toyota.model

case class AverageVote(sumNumVotes: BigDecimal, countNumVotes: BigDecimal)
