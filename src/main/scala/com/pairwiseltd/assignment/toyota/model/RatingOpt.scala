package com.pairwiseltd.assignment.toyota.model

case class RatingOpt(tconst: Option[String], averageRating: Option[BigDecimal], numVotes: Option[Int])
