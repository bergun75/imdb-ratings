package com.pairwiseltd.assignment.toyota.model

case class Rating(tconst: String, averageRating: BigDecimal, numVotes: Int)

object Rating {
  def apply(rating: RatingOpt): Rating =
    new Rating(tconst = rating.tconst.getOrElse(""),
      averageRating = rating.averageRating.getOrElse(BigDecimal(Int.MinValue)),
      numVotes = rating.numVotes.getOrElse(Int.MinValue))
}
