package com.pairwiseltd.assignment.toyota.model

case class EnrichedTitle(tconst: String, ranking: BigDecimal, primaryTitle: String, originalTitle: String,
                         mostCreditedName: String)

object EnrichedTitle {
  def apply(title: EnrichedRawTitleWithRanking): EnrichedTitle =
    new EnrichedTitle(tconst = title.tconst,
      ranking = title.ranking,
      primaryTitle = title.primaryTitle,
      originalTitle = title.originalTitle,
      mostCreditedName = title.mostCreditedName)
}
