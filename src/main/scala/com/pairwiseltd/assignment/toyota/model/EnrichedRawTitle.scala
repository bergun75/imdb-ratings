package com.pairwiseltd.assignment.toyota.model

case class EnrichedRawTitle(tconst: String, averageRating: BigDecimal, numVotes: Int, primaryTitle: String,
                            originalTitle: String, mostCreditedName: String)

object EnrichedRawTitle {
  def apply(rating: Rating): EnrichedRawTitle =
    new EnrichedRawTitle(tconst = rating.tconst,
      averageRating = rating.averageRating,
      numVotes = rating.numVotes,
      primaryTitle = "",
      originalTitle = "",
      mostCreditedName = "")

  def apply(rating: EnrichedRawTitleWithOrdering): EnrichedRawTitle = {
    new EnrichedRawTitle(tconst = rating.tconst,
      averageRating = rating.averageRating,
      numVotes = rating.numVotes,
      primaryTitle = rating.primaryTitle,
      originalTitle = rating.originalTitle,
      mostCreditedName = rating.primaryName)
  }
}
