package com.pairwiseltd.assignment.toyota

import com.pairwiseltd.assignment.toyota.model.{Name, Principal, Rating, RatingOpt, Title}
import com.pairwiseltd.assignment.toyota.schema.Schemas
import com.pairwiseltd.assignment.toyota.transformations.TitleTransformations
import org.apache.spark.sql.{Column, DataFrame, Dataset, SparkSession}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.streaming.Trigger
import org.apache.spark.sql.types.DecimalType

object ImdbRatings {
  val spark: SparkSession = SparkSession.builder()
    .appName("ImdbRatings")
    .master("local[2]")
    .getOrCreate()

  def main(args: Array[String]): Unit = {
    spark.sparkContext.setLogLevel("WARN")
    import spark.implicits._
    // TODO These normally be external parameters from args. Read README on #How To Run
    val topNumber = 10
    val minVotesToConsider = 500
    val (titles, ratings, principals, names) =
      readAllFiles(titlesPath = "src/main/resources/externalData/title.basics.tsv",
        ratingsPath = "src/main/resources/externalData/title.ratings.tsv",
        principalsPath = "src/main/resources/externalData/title.principals.tsv",
        namesPath = "src/main/resources/externalData/name.basics.tsv")

    val transformations = new TitleTransformations(spark, topNumber, minVotesToConsider)
    val enrichedTitles = transformations.extractEnrichedTitles(staticRatings = ratings,
      titles = titles,
      principals = principals,
      names = names)
    val averageVoteStats = transformations.sumOfVotes(ratings)
    val topStaticTitles = transformations.topEnrichedTitles(enrichedTitles, averageVoteStats)

    spark.readStream
      .format("socket") // TODO for the sake of this exercise to test quickly I chose socket
      .option("host", "localhost")
      .option("port", 54321)
      .load()
      .select(from_csv(col("value"), Schemas.rating, Map("delimiter" -> "\t", "mode" -> "PERMISSIVE"))
        .as("rating"))
      .selectExpr("rating.tconst as tconst", "rating.averageRating as averageRating", "rating.numVotes as numVotes")
      .as[RatingOpt].map(Rating(_))
      .filter(x => !x.tconst.isEmpty &&
        x.averageRating != BigDecimal(Int.MinValue) &&
        x.numVotes != Int.MinValue)
      .groupByKey(_.tconst)
      .reduceGroups {
        (r1, r2) =>
          r1.copy(numVotes = math.max(r1.numVotes, r2.numVotes),
            averageRating = if (r1.numVotes > r2.numVotes) r1.averageRating else r2.averageRating)
      }
      .map(_._2)
      .writeStream
      .foreachBatch { (batch: Dataset[Rating], batchId: Long) =>
        val batchAgg = transformations.aggregateStreamBatch(batch, topStaticTitles, averageVoteStats)
        batchAgg.show(false)
        // TODO just outputting to console for the sake of this exercise I would be writing to distributed file system or DB
        // normally
      }
      .outputMode("complete")
      .trigger(Trigger.ProcessingTime("15 seconds")) // TODO I would set value this dynamically on cluster environment
      .start()
      .awaitTermination()

  }

  private def readAllFiles(titlesPath: String,
                           ratingsPath: String,
                           principalsPath: String,
                           namesPath: String) = {
    import spark.implicits._
    val ratings = readTsvFile(ratingsPath)
      .select(col("tconst"),
        col("averageRating").cast(DecimalType(4, 2)),
        col("numVotes"))
      .as[Rating]
      .repartition(col("tconst"))

    val titles = readTsvFile(titlesPath)
      .select(col("tconst"),
        col("titleType"),
        col("primaryTitle"),
        col("originalTitle"))
      .as[Title]
      .repartition(col("tconst"))

    val principals = readTsvFile(principalsPath)
      .select(col("tconst"),
        col("ordering"),
        col("nconst"))
      .as[Principal]
      .repartition(col("tconst"))

    val names = readTsvFile(namesPath)
      .select(col("nconst"),
        col("primaryName"))
      .as[Name]
    (titles, ratings, principals, names)
  }

  private def readTsvFile(path: String, columns: Column*): DataFrame = spark.read
    .option("inferSchema", "true")
    .option("header", "true")
    .option("delimiter", "\t")
    .csv(path)

}
