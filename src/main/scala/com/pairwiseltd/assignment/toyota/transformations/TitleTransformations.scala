package com.pairwiseltd.assignment.toyota.transformations

import com.pairwiseltd.assignment.toyota.model._
import org.apache.log4j.Logger
import org.apache.spark.broadcast.Broadcast
import org.apache.spark.sql.catalyst.plans.LeftOuter
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.DecimalType
import org.apache.spark.sql.{Dataset, SparkSession}
import org.apache.spark.storage.StorageLevel

/**
 * This class contains all static and stream batch transformations
 *
 * @param spark              spark session
 * @param limit              number to limit result set output
 * @param minVotesToConsider minimum numbers of votes for considering ranking of a movie.
 */
class TitleTransformations(spark: SparkSession,
                           limit: Int,
                           minVotesToConsider: Int) extends Serializable {
  @transient private lazy val log = Logger.getLogger(getClass.getName)

  /**
   *
   * @param ratingsFromStream batch from the stream reader
   * @param topStaticTitles   initial ordered op static titles.
   *                          Contains [[limit]] * 20 since we are not interested in changes lower rankings
   * @param averageVoteStats  broadcasted initial averageVoteStats
   * @return Dataset[EnrichedTitle] result set to output sorted and limited with [[limit]]
   */
  final def aggregateStreamBatch(ratingsFromStream: Dataset[Rating],
                                 topStaticTitles: Dataset[EnrichedRawTitleWithRanking],
                                 // contains topNumber * 20 since we are not interested in changes lower rankings
                                 averageVoteStats: Broadcast[Array[AverageVote]]): Dataset[EnrichedTitle] = {
    log.info("Triggered batch")
    import spark.implicits._
    val movieRatingsFromStream = ratingsFromStream.filter(_.numVotes >= minVotesToConsider)
      .repartition(col("tconst"))
    val newAverageVoteStats = sumOfVotes(movieRatingsFromStream.groupByKey(_.tconst)
      .reduceGroups { (x1, x2) =>
        x1.copy(numVotes = math.max(x1.numVotes, x2.numVotes),
          averageRating = if (x1.numVotes > x2.numVotes) x1.averageRating else x2.averageRating)
      }.map(_._2))

    val updatedAverageVoteStats = averageVoteStats.value.flatMap(x =>
      newAverageVoteStats.value.map(y => AverageVote(sumNumVotes = x.sumNumVotes + y.sumNumVotes,
        countNumVotes = x.countNumVotes + y.countNumVotes))).head
    val averageVotes = updatedAverageVoteStats.sumNumVotes / updatedAverageVoteStats.countNumVotes
    val streamRatingsFakeEnriched = movieRatingsFromStream.filter(_.numVotes >= minVotesToConsider)
      .select(col("tconst"),
        col("averageRating"),
        col("numVotes"),
        lit("").as("primaryTitle"), // Remember we don't care about lower ranking so this will be -
        lit("").as("originalTitle"), // replaced from the static ratings.
        lit("").as("mostCreditedName"))
      .as[EnrichedRawTitle].map(EnrichedRawTitleWithRanking(_, averageVotes))

    val combinedStaticAndStreamBatch = topStaticTitles
      .map(x => x.copy(ranking = x.averageRating * x.numVotes / averageVotes))
      .union(streamRatingsFakeEnriched)

    val updatedTitleRankings = combinedStaticAndStreamBatch.groupByKey(_.tconst)
      .reduceGroups { (x1, x2) =>
        EnrichedRawTitleWithRanking(
          tconst = x1.tconst,
          ranking = if (x1.numVotes > x2.numVotes) x1.ranking else x2.ranking,
          numVotes = math.max(x1.numVotes, x2.numVotes),
          averageRating = if (x1.numVotes > x2.numVotes) calculateRanking(x1.numVotes, x1.averageRating, averageVotes)
          else calculateRanking(x2.numVotes, x2.averageRating, averageVotes),
          primaryTitle = takeValidString(x1.primaryTitle, x2.primaryTitle),
          originalTitle = takeValidString(x1.originalTitle, x2.originalTitle),
          mostCreditedName = takeValidString(x1.mostCreditedName, x2.mostCreditedName))
      }.map(_._2).filter(!_.primaryTitle.isEmpty)
    val orderedTitleRankings = updatedTitleRankings
      .orderBy(col("ranking").desc)
    log.info("Batch complete")
    orderedTitleRankings.map(EnrichedTitle(_)).limit(limit)
  }

  /**
   * static batch transformation for enriching titles, all sources are from static sources
   *
   * @param staticRatings initial ratings
   * @param titles initial titles from
   * @param principals initial principals
   * @param names initial names
   * @return enriched dataset
   */
  final def extractEnrichedTitles(staticRatings: Dataset[Rating],
                            titles: Dataset[Title],
                            principals: Dataset[Principal],
                            names: Dataset[Name]): Dataset[EnrichedRawTitle] = {
    import spark.implicits._
    // somewhat straight forward not wasted time writing tests but that doesn't mean I wouldn't
    // write tests for this in real production.
    staticRatings
      .join(titles.filter(_.titleType.toLowerCase == "movie"), staticRatings("tconst") === titles("tconst"))
      .join(principals, staticRatings("tconst") === principals("tconst"), LeftOuter.toString)
      .join(names, principals("nconst") === names("nconst"), LeftOuter.toString)
      .select(
        staticRatings("tconst"),
        staticRatings("averageRating").cast(DecimalType(4, 2)),
        staticRatings("numVotes"),
        titles("primaryTitle"),
        titles("originalTitle"),
        principals("ordering"),
        names("primaryName")
      ).as[EnrichedRawTitleWithOrdering] // shall I consider short movies don't know
      .groupByKey(_.tconst)
      .reduceGroups { (x1, x2) =>
        x1.copy(
          ordering = Option(math.min(x1.ordering.getOrElse(Integer.MAX_VALUE), x2.ordering.getOrElse(Integer.MAX_VALUE))),
          primaryName =
            if (x1.ordering.getOrElse(Integer.MAX_VALUE) < x2.ordering.getOrElse(Integer.MAX_VALUE)) x1.primaryName
            else x2.primaryName,
          numVotes = math.max(x1.numVotes, x2.numVotes),
          // I handled this in case the static rating file has more than 1 entry for ratings
          averageRating = if (x1.numVotes > x2.numVotes) x1.averageRating else x2.averageRating
        )
      }.map(_._2).map(EnrichedRawTitle(_))
  }

  /**
   * transformation for initial static titles with order by ranking desc, all sources are from static sources
   *
   * @param titles initial titles
   * @param averageVotesStatsBcst average votes broadcasted value of 1 row.
   * @return result dataset of top sorted titles
   */
  final def topEnrichedTitles(titles: Dataset[EnrichedRawTitle],
                        averageVotesStatsBcst: Broadcast[Array[AverageVote]]): Dataset[EnrichedRawTitleWithRanking] = {
    import spark.implicits._
    // somewhat straight forward not wasted time writing tests but that doesn't mean I wouldn't
    // write tests for this in real production.
    val averageVoteStats = averageVotesStatsBcst.value.head
    val averageVote = averageVoteStats.sumNumVotes / averageVoteStats.countNumVotes
    val rankedTitles = titles.filter(_.numVotes >= minVotesToConsider)
      .map(EnrichedRawTitleWithRanking(_, averageVote))
    rankedTitles
      .orderBy(col("ranking").desc).limit(limit * 20).persist()
  }

  /**
   * transformation for calculating average votes statistics. Includes an action to collect 1 row of data to driver
   * and broadcast it to memory of all executors for reference.
   *
   * @param ratings works both with static initial ratings and ratings from stream batch
   * @return broadcasted value of average vote stats
   */
  final def sumOfVotes(ratings: Dataset[Rating]): Broadcast[Array[AverageVote]] = {
    import spark.implicits._
    spark.sparkContext.broadcast(ratings.select(sum(col("numVotes")).as("sumNumVotes"),
      count(col("numVotes")).as("countNumVotes")).as[AverageVote]
      .map(x => x.copy(sumNumVotes = Option(x.sumNumVotes).getOrElse(BigDecimal(0)),
        countNumVotes = Option(x.countNumVotes).getOrElse(BigDecimal(1))))
      .collect)
  }

  // Helper functions
  private def calculateRanking(numVotes: Int, averageRating: BigDecimal, averageVote: BigDecimal): BigDecimal =
    numVotes * averageRating / averageVote


  private def takeValidString(x1: String, x2: String): String =
    if (x1.isEmpty) x2 else x1
}
