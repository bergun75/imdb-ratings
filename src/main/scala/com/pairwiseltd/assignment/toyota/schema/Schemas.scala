package com.pairwiseltd.assignment.toyota.schema

import org.apache.spark.sql.types._

object Schemas {
  val rating: StructType = StructType(Array(StructField("tconst", StringType, nullable = false),
    StructField("averageRating", DecimalType(4, 2), nullable = false),
    StructField("numVotes", IntegerType, nullable = false)))
}
